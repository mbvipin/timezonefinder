package com.timezone.constants;

public class Constants {
	
	public static final String GEO_NAME_URL = "http://api.geonames.org/timezoneJSON";
	
	public static final String HTTP_REQUEST_GET="GET";
	
	public static final String CONTENTTYPE="Content-Type";
	
	public static final String JSON="application/json";
	
	public static final String NEWLINE="\n";
	
	public static final String COMMA=",";
	
	public static final String TIMEZONE="timezoneId";
	
	public static final String TIME="time";

}
