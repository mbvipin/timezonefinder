package com.timezone.processor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.timezone.constants.Constants.COMMA;
import static com.timezone.constants.Constants.GEO_NAME_URL;
import static com.timezone.constants.Constants.CONTENTTYPE;
import static com.timezone.constants.Constants.HTTP_REQUEST_GET;
import static com.timezone.constants.Constants.JSON;
import static com.timezone.constants.Constants.NEWLINE;
import static com.timezone.constants.Constants.TIME;
import static com.timezone.constants.Constants.TIMEZONE;

/**
 * @author Vipin
 *
 */
public class TimezoneMain {

	final static Logger logger = Logger.getLogger(TimezoneMain.class);

	PrintWriter pw = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length > 0) {
			String fileName = args[0];

			TimezoneMain timezoneFinder = new TimezoneMain();

			timezoneFinder.readAndProcess(fileName);
		}

		else {
			logger.error("Need the location of file to process the input");
		}
	}

	/**
	 * @param fileName
	 */
	private void readAndProcess(String fileName) {

		String csvFile = fileName;
		String line = "";
		String cvsSplitBy = COMMA;

		// Initializing the file to write the output
		try {

			File inputFile = new File(fileName);
			// Now get the path
			String inputDir = inputFile.getParent();
			pw = new PrintWriter(new File(inputDir + "/output.csv"));
		} catch (FileNotFoundException e1) {
			logger.error("Couldn't create the output file. An exception occured "
					+ e1.getMessage());
			System.exit(1);
		}

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] values = line.split(cvsSplitBy);

				logger.debug("Latitude :" + values[1]);
				logger.debug("Longitude :" + values[1]);

				// Calling Geo Names API with required parameters
				callAPI(values[1], values[2]);

			}

		} catch (IOException e) {

			logger.error("Exception occured while reading the input file"
					+ e.getMessage());
		}

		finally {
			// Closing the output file writer after processing all lines
			pw.close();
		}

	}

	/**
	 * @param lat
	 * @param lng
	 */
	private void callAPI(String lat, String lng) {

		try {
			URL httpURL = new URL(getURL(lat, lng));

			HttpURLConnection conn = (HttpURLConnection) httpURL
					.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(HTTP_REQUEST_GET);
			conn.setRequestProperty(CONTENTTYPE, JSON);

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			ObjectMapper mapper = new ObjectMapper();
			JsonNode readTree = mapper.readTree(conn.getInputStream());
			Map<String, Object> convertValue = mapper.convertValue(readTree,
					new TypeReference<Map<String, Object>>() {
					});

			StringBuilder sb = new StringBuilder();
			sb.append(convertValue.get(TIMEZONE));
			sb.append(COMMA);
			sb.append(convertValue.get(TIME));
			sb.append(NEWLINE);
			pw.write(sb.toString());

			conn.disconnect();

		} catch (MalformedURLException e) {

			logger.error("URL created to call the API is not valid "
					+ e.getMessage());
		}

		catch (IOException e) {

			logger.error("An exception occured while processing API output "
					+ e.getMessage());

		}

	}

	/**
	 * @param lat
	 * @param lng
	 * @return API URL with parameters
	 */
	private String getURL(String lat, String lng) {
		StringBuffer url = new StringBuffer();

		// TODO As an improvement, we can use message formating to populate
		// query string parameters
		url.append(GEO_NAME_URL);
		url.append("?");
		url.append("lat=" + lat);
		url.append("&");
		url.append("lng=" + lng);
		url.append("&");
		url.append("username=mbvipin");

		return url.toString();
	}

}
